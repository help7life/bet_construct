%%%-------------------------------------------------------------------
%%% @author alex
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Апр. 2019 13:40
%%%-------------------------------------------------------------------
-module(ticker_worker).
-author("alex").

-behaviour(gen_server).

%% API
-export([
  start_link/0,
  price/1
]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {
  host::binary(),
  port::integer(),
  page::list(),
  tab::reference(),
  pid::pid()
}).


%%%===================================================================
%%% API
%%%===================================================================
price(ProductId) ->
  gen_server:call(?SERVER, {price, ProductId}).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  self() ! init,
  {ok, #state{
    host = "localhost",
    port = list_to_integer(application:get_env(bet_construct, port, "9222")),
    page = "https://pro.coinbase.com/",
    tab = ets:new(tickers, [protected])
  }}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call({price, ProductId}, _From, State) ->
  Response = case ets:lookup(State#state.tab, ProductId) of
               [{ProductId, Price}] ->
                 Price;
               [] ->
                 <<"no quotes">>
             end,
  {reply, Response, State};
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(init, State) ->
  Host = State#state.host,
  Port = State#state.port,
  {ok, Pid} = gun:open(Host, Port),
  {ok, http} = gun:await_up(Pid,10000),
  gun:get(Pid, "/json/new?" ++ State#state.page),
  %%  _MRef = monitor(process, Pid),
  {noreply, State#state{pid = Pid}};
handle_info({gun_response, _ConnPid, _StreamRef, nofin, _Status, _Headers}, State) ->
  error_logger:info_msg("Response from chrome"),
  {noreply, State};
handle_info({gun_data, _ConnPid, _StreamRef, fin, Data}, State) ->
   gun:ws_upgrade(State#state.pid, <<"/devtools/page/", ((maps:get(<<"id">>, jsx:decode(Data, [return_maps]))))/binary>>, [], #{compress => true}),
  {noreply, State};
handle_info({gun_ws, _ConnPid, _StreamRef, {text, Frame}}, State) ->
  case parse(Frame) of
    ok ->
      ok;
    Ticker ->
      ets:insert(State#state.tab, Ticker)
  end,
  {noreply, State};

handle_info({gun_upgrade, _ConnPid, _StreamRef, [<<"websocket">>], _}, State) ->
  error_logger:info_msg("Connection upgrade to websocket"),
  gun:ws_send(State#state.pid, {text, <<"{\"id\":1,\"method\":\"Network.enable\",\"params\":{\"maxPostDataSize\":65536}}">>}),
  {noreply, State};

handle_info({gun_down, _ConnPid, ws, closed, [], []}, State) ->
  error_logger:info_msg("Current websocket connection is closed "),
  {noreply, State};

%%handle_info({gun_down, _ConnPid, http, closed, [], []}, State) ->
%%  error_logger:info_msg("Current http connection is closed "),
%%  {noreply, State};
%%handle_info({gun_up, _ConnPid, http}, State) ->
%%  error_logger:info_msg("Connection up to http"),
%%  {noreply, State};
%%handle_info({'DOWN', _Mref, process, _ConnPid, Reason}, State) ->
%%  error_logger:error_msg("Oops!"),
%%  exit(Reason),
%%  {noreply, State};

handle_info(Info, State) ->
  error_logger:info_msg("Unhandled message: ", [Info]),
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, State) ->
  gun:close(State#state.pid),
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
parse(Frame) ->
  case jsx:decode(Frame, [return_maps]) of
    #{<<"params">> := #{<<"response">> :=  #{<<"payloadData">> :=PayloadData}}} ->
      case jsx:decode(PayloadData, [return_maps]) of
        #{<<"type">> := <<"ticker">>, <<"product_id">> := ProductId, <<"price">> := Price} ->
          error_logger:info_msg("ProductId : ~p ,Price ~p", [ProductId, Price]),
          {ProductId, Price};
        _ ->
          ok
      end;

    _ ->
      ok
  end.