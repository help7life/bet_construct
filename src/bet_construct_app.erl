-module(bet_construct_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
	spawn_link(fun()->os:cmd("google-chrome --remote-debugging-port="++application:get_env(bet_construct,port,"9222")++" --headless --disable-gpu") end),
	bet_construct_sup:start_link().

stop(_State) ->
	[_,Pid|_]=string:tokens(os:cmd("ps aux | grep port="++application:get_env(bet_construct,port,"9222")++" | grep -v grep")," "),
	os:cmd("kill "++Pid),
	ok.
