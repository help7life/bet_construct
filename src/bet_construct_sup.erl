-module(bet_construct_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	Procs = [
		{ticker_worker,
			{ticker_worker, start_link, []},
			permanent,
			2000,
			worker,
			[ticker_worker]}
	],
	{ok, {{one_for_one, 1, 5}, Procs}}.
